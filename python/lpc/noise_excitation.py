import numpy as np
import scipy.signal as ss

def noise_excitation(a, g, block_size):
    src = np.sqrt(g)*np.random.randn(block_size, 1) # noise

    b = np.concatenate([np.array([-1]), a])

    x_hat = ss.lfilter([1], b.T, src.T).T

    # convert Nx1 matrix into a N vector
    return np.squeeze(x_hat)
