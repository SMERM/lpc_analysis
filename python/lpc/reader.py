import numpy as np
import scipy.io.wavfile

def reader(filename):
	(sr, signal) = scipy.io.wavfile.read(filename)
	signal = np.array(signal)
	signal = 0.99*signal/np.max(np.abs(signal))
	return (sr, signal)
