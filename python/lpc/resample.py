import scipy.signal as ss

def resample(signal, target_sample_rate, orig_sample_rate):
	target_size = int(len(signal)*target_sample_rate/orig_sample_rate)

	signal = ss.resample(signal, target_size)
	sample_rate = target_sample_rate

	return (sample_rate, signal)
